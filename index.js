console.clear()
console.log('start')

// boilderplate
const cnv = document.createElement('canvas')
cnv.width = 600
cnv.height = 500
const ctx = cnv.getContext('2d')

document.body.appendChild(cnv)

loop()

function loop() {
	setInterval(render, 1000 / 24)
}

function render() {
	clearCanvas();
}

function clearCanvas() {
	ctx.fillStyle = '#444'
	ctx.fillRect(0, 0, cnv.width, cnv.height)
}

function getMousePos(canvas, evt) {
	var rect = canvas.getBoundingClientRect();
	return {
		x: evt.clientX - rect.left,
		y: evt.clientY - rect.top
	};
}


